# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# Get gitlab password file
gsutil cp gs://${project_id}/ops/acm/gitlab_password.txt gitlab_password.txt
export GITLAB_PASSWORD=$(cat gitlab_password.txt)
export DOMAIN=endpoints.${project_id}.cloud.goog

# Get an Oauth Token to authenticate with GitLab API
cat > auth.txt <<EOF
grant_type=password&username=root&password=${GITLAB_PASSWORD}
EOF
export GITLAB_OAUTH_TOKEN=$(curl -k --data "@auth.txt" --request POST https://gitlab.${DOMAIN}/oauth/token | jq -r .access_token)
# Get ACM repo project ID
export ACM_GROUP_ID=$(curl -k -s "https://gitlab.${DOMAIN}/api/v4/groups?search=platform-admins&access_token=${GITLAB_OAUTH_TOKEN}" | jq -r .[0].id)
# Get runner registration token from GitLab
export ACM_RUNNER_TOKEN=$(curl -k -s "https://gitlab.${DOMAIN}/api/v4/groups/${ACM_GROUP_ID}?access_token=${GITLAB_OAUTH_TOKEN}" | jq -r .runners_token)
export ACM_RUNNER_TOKEN_BASE64=$(echo ${ACM_RUNNER_TOKEN} | base64)

sed -e s/ACM_RUNNER_TOKEN/${ACM_RUNNER_TOKEN_BASE64}/g ./acm-repo/namespaces/istio-system/gitlab-runner-secret.yaml_tmpl > ./acm-repo/namespaces/istio-system/gitlab-runner-secret.yaml
