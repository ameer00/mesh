resource "google_service_account" "gitlab-sa" {
  account_id   = "gitlab-sa"
  display_name = "gitlab sa"
  project      = var.project_id
}

resource "google_project_iam_member" "gitlab-sa-owner-iam" {
  project = var.project_id
  role    = "roles/editor"
  member  = "serviceAccount:${google_service_account.gitlab-sa.email}"
}

resource "google_project_iam_member" "gitlab-sa-cluster-admin-iam" {
  project = var.project_id
  role    = "roles/container.admin"
  member  = "serviceAccount:${google_service_account.gitlab-sa.email}"
}

resource "google_service_account_key" "gitlab-sa-key" {
  service_account_id = google_service_account.gitlab-sa.id
}

resource "local_file" "gitlab-sa-json-file" {
    content     = base64decode(google_service_account_key.gitlab-sa-key.private_key)
    filename = "${path.module}/myaccount.json"
}

resource "local_file" "gitlab-sa-json-file-base64" {
    content     = google_service_account_key.gitlab-sa-key.private_key
    filename = "${path.module}/myaccount-base64.json"
}

resource "google_storage_bucket_object" "gitlab-sa-key-object" {
  name   = "ops/acm/gitlab-sa.json"
  source = "${path.module}/myaccount.json"
  bucket = var.project_id
  depends_on = [local_file.gitlab-sa-json-file]
}

resource "google_storage_bucket_object" "gitlab-sa-key-object-base64" {
  name   = "ops/acm/gitlab-sa-base64.json"
  source = "${path.module}/myaccount-base64.json"
  bucket = var.project_id
  depends_on = [local_file.gitlab-sa-json-file-base64]
}
