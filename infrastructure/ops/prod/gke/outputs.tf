# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

output "vpc_prod1_gke_1_name" { value = "${module.p-r1-abc-ops1.name}" }
output "vpc_prod1_gke_1_location" { value = "${module.p-r1-abc-ops1.location}" }

output "vpc_prod1_gke_2_name" { value = "${module.p-r1-b-app1.name}" }
output "vpc_prod1_gke_2_location" { value = "${module.p-r1-b-app1.location}" }

output "vpc_prod1_gke_3_name" { value = "${module.p-r1-c-app2.name}" }
output "vpc_prod1_gke_3_location" { value = "${module.p-r1-c-app2.location}" }

output "vpc_prod1_gke_4_name" { value = "${module.p-r2-abc-app3.name}" }
output "vpc_prod1_gke_4_location" { value = "${module.p-r2-abc-app3.location}" }

output "vpc_prod2_gke_5_name" { value = "${module.p-r3-abc-app4.name}" }
output "vpc_prod2_gke_5_location" { value = "${module.p-r3-abc-app4.location}" }
