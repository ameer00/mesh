# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

output "vpc_dev_stage_gke_6_name" { value = "${module.s-r4-abc-stage1.name}" }
output "vpc_dev_stage_gke_6_location" { value = "${module.s-r4-abc-stage1.location}" }

output "vpc_dev_stage_gke_7_name" { value = "${module.d-r5-abc-dev1.name}" }
output "vpc_dev_stage_gke_7_location" { value = "${module.d-r5-abc-dev1.location}" }
