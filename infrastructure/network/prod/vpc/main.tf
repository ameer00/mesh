#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

###################  NETWORK  #######################

# Create vpc-prod-1
module "vpc-prod-1" {
  source          = "terraform-google-modules/network/google"
  version         = "~> 2.3"
  project_id      = var.project_id
  network_name    = var.vpc_prod_1_name
  routing_mode    = var.routing_mode

  subnets = [
    {
      subnet_name      = var.vpc_prod1_subnet_01_name
      subnet_ip        = var.vpc_prod1_subnet_01_ip
      subnet_region    = var.vpc_prod1_subnet_01_region
      subnet_flow_logs = "true"
      description      = var.vpc_prod1_subnet_01_description
    },
    {
      subnet_name      = var.vpc_prod1_subnet_02_name
      subnet_ip        = var.vpc_prod1_subnet_02_ip
      subnet_region    = var.vpc_prod1_subnet_02_region
      subnet_flow_logs = "true"
      description      = var.vpc_prod1_subnet_02_description
    },
    {
      subnet_name      = var.vpc_prod1_subnet_03_name
      subnet_ip        = var.vpc_prod1_subnet_03_ip
      subnet_region    = var.vpc_prod1_subnet_03_region
      subnet_flow_logs = "true"
      description      = var.vpc_prod1_subnet_03_description
    },
    {
      subnet_name      = var.vpc_prod1_subnet_04_name
      subnet_ip        = var.vpc_prod1_subnet_04_ip
      subnet_region    = var.vpc_prod1_subnet_04_region
      subnet_flow_logs = "true"
      description      = var.vpc_prod1_subnet_04_description
    },
  ]

  secondary_ranges = {
    (var.vpc_prod1_subnet_01_name) = [
      {
        range_name    = var.vpc_prod1_subnet_01_secondary_svc_1_name
        ip_cidr_range = var.vpc_prod1_subnet_01_secondary_svc_1_range
      },
      {
        range_name    = var.vpc_prod1_subnet_01_secondary_pod_name
        ip_cidr_range = var.vpc_prod1_subnet_01_secondary_pod_range
      },
    ]
    (var.vpc_prod1_subnet_02_name) = [
      {
        range_name    = var.vpc_prod1_subnet_02_secondary_svc_1_name
        ip_cidr_range = var.vpc_prod1_subnet_02_secondary_svc_1_range
      },
      {
        range_name    = var.vpc_prod1_subnet_02_secondary_pod_name
        ip_cidr_range = var.vpc_prod1_subnet_02_secondary_pod_range
      },
    ]
    (var.vpc_prod1_subnet_03_name) = [
      {
        range_name    = var.vpc_prod1_subnet_03_secondary_svc_1_name
        ip_cidr_range = var.vpc_prod1_subnet_03_secondary_svc_1_range
      },
      {
        range_name    = var.vpc_prod1_subnet_03_secondary_pod_name
        ip_cidr_range = var.vpc_prod1_subnet_03_secondary_pod_range
      },
    ]
    (var.vpc_prod1_subnet_04_name) = [
      {
        range_name    = var.vpc_prod1_subnet_04_secondary_svc_1_name
        ip_cidr_range = var.vpc_prod1_subnet_04_secondary_svc_1_range
      },
      {
        range_name    = var.vpc_prod1_subnet_04_secondary_pod_name
        ip_cidr_range = var.vpc_prod1_subnet_04_secondary_pod_range
      },
    ]
  }
}

module "vpc-prod-2" {
  source          = "terraform-google-modules/network/google"
  version         = "~> 2.3"
  project_id      = var.project_id
  network_name    = var.vpc_prod_2_name
  routing_mode    = var.routing_mode

  subnets = [
    {
      subnet_name      = var.vpc_prod2_subnet_01_name
      subnet_ip        = var.vpc_prod2_subnet_01_ip
      subnet_region    = var.vpc_prod2_subnet_01_region
      subnet_flow_logs = "true"
      description      = var.vpc_prod2_subnet_01_description
    }
  ]
 
  secondary_ranges = {
    (var.vpc_prod2_subnet_01_name) = [
      {
        range_name    = var.vpc_prod2_subnet_01_secondary_svc_1_name
        ip_cidr_range = var.vpc_prod2_subnet_01_secondary_svc_1_range
      },
      {
        range_name    = var.vpc_prod2_subnet_01_secondary_pod_name
        ip_cidr_range = var.vpc_prod2_subnet_01_secondary_pod_range
      },
    ]
  }
}

# create firewall rules to allow-all inernally for vpc-prod-1 
module "vpc-prod-1-net-firewall" {
  source                  = "terraform-google-modules/network/google//modules/fabric-net-firewall"
  project_id              = var.project_id
  network                 = module.vpc-prod-1.network_name
  internal_ranges_enabled = true
  internal_ranges         = ["10.0.0.0/8"]
  internal_allow = [
    { "protocol" : "all" },
  ]
}

# create firewall rules to allow-all inernally for vpc-prod-2 
module "vpc-prod-2-net-firewall" {
  source                  = "terraform-google-modules/network/google//modules/fabric-net-firewall"
  project_id              = var.project_id
  network                 = module.vpc-prod-2.network_name
  internal_ranges_enabled = true
  internal_ranges         = ["10.0.0.0/8"]
  internal_allow = [
    { "protocol" : "all" },
  ]
}
