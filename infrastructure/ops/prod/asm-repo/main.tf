# Archive used to detect changes in repo
data "archive_file" "asm_repo" {
  type        = "zip"
  source_dir  = "asm-repo/"
  output_path = "data.zip"
}

resource "null_resource" "exec_create_asm_repo" {
  provisioner "local-exec" {
    interpreter = ["bash", "-exc"]
    command     = "${path.module}/push_asm_repo.sh" 
    environment = {
      project_id = var.project_id
    }
  }

  triggers = {
    script_sha1          = sha1(file("push_asm_repo.sh"))
    data_sha1            = data.archive_file.asm_repo.output_sha
  }
}

