# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

###################  VPC PROD 1 VARIABLES  #######################

# Project ID and VPCs
variable "project_id" {}
variable "vpc_dev_stage_name" {}
variable "routing_mode" {}

# vpc-dev-stage subnets and secondary subnets
variable "vpc_dev_stage_subnet_01_name" {}
variable "vpc_dev_stage_subnet_01_ip" {}
variable "vpc_dev_stage_subnet_01_region" {}
variable "vpc_dev_stage_subnet_01_description" {}

variable "vpc_dev_stage_subnet_02_name" {}
variable "vpc_dev_stage_subnet_02_ip" {}
variable "vpc_dev_stage_subnet_02_region" {}
variable "vpc_dev_stage_subnet_02_description" {}

variable "vpc_dev_stage_subnet_01_secondary_svc_1_name" {}
variable "vpc_dev_stage_subnet_01_secondary_svc_1_range" {}
variable "vpc_dev_stage_subnet_01_secondary_pod_name" {}
variable "vpc_dev_stage_subnet_01_secondary_pod_range" {}

variable "vpc_dev_stage_subnet_02_secondary_svc_1_name" {}
variable "vpc_dev_stage_subnet_02_secondary_svc_1_range" {}
variable "vpc_dev_stage_subnet_02_secondary_pod_name" {}
variable "vpc_dev_stage_subnet_02_secondary_pod_range" {}
