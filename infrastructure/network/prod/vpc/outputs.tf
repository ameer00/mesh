output "vpc_prod1_subnets_self_links" { value = "${module.vpc-prod-1.subnets_self_links}" }
output "vpc_prod1_subnets_names" { value = "${module.vpc-prod-1.subnets_names}" }
output "vpc_prod1_network_name" { value = "${module.vpc-prod-1.network_name}" }
output "vpc_prod1_network_self_link" { value = "${module.vpc-prod-1.network_self_link}" }

output "vpc_prod2_subnets_self_links" { value = "${module.vpc-prod-2.subnets_self_links}" }
output "vpc_prod2_subnets_names" { value = "${module.vpc-prod-2.subnets_names}" }
output "vpc_prod2_network_name" { value = "${module.vpc-prod-2.network_name}" }
output "vpc_prod2_network_self_link" { value = "${module.vpc-prod-2.network_self_link}" }
