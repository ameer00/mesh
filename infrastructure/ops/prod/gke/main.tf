# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

data "google_container_engine_versions" "region_01" {
  project        = var.project_id
  location       = var.vpc_prod1_gke_1_region
  version_prefix = var.kubernetes_version
}

data "google_container_engine_versions" "region_02" {
  project        = var.project_id
  location       = var.vpc_prod1_gke_4_region
  version_prefix = var.kubernetes_version
}

data "google_container_engine_versions" "region_03" {
  project        = var.project_id
  location       = var.vpc_prod2_gke_5_region
  version_prefix = var.kubernetes_version
}

# p-r1-abc-ops1 - GKE regional cluster in region 1 for ASM control plane
module "p-r1-abc-ops1" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/beta-public-cluster"
  project_id                 = var.project_id
  kubernetes_version         = data.google_container_engine_versions.region_01.latest_master_version
  name                       = var.vpc_prod1_gke_1
  region                     = var.vpc_prod1_gke_1_region
  zones                      = var.vpc_prod1_gke_1_zones
  network                    = var.vpc_prod1
  subnetwork                 = var.vpc_prod1_subnet_01
  ip_range_pods              = var.vpc_prod1_subnet_01_pods
  ip_range_services          = var.vpc_prod1_subnet_01_svc
  horizontal_pod_autoscaling = true
  network_policy             = true
  node_metadata              = "GKE_METADATA_SERVER"
  identity_namespace         = "${var.project_id}.svc.id.goog"
  monitoring_service         = "monitoring.googleapis.com/kubernetes"
  logging_service            = "logging.googleapis.com/kubernetes"

  node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = var.node_pools_machine_type
      min_count          = 1
      max_count          = 3
      local_ssd_count    = 0
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      # service_account    = "project-service-account@<PROJECT ID>.iam.gserviceaccount.com"
      preemptible        = false
      initial_node_count = 2
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }
}

# p-r1-b-app1 - GKE zonal cluster for apps
module "p-r1-b-app1" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/beta-public-cluster"
  project_id                 = var.project_id
  kubernetes_version         = data.google_container_engine_versions.region_01.latest_master_version
  name                       = var.vpc_prod1_gke_2
  region                     = var.vpc_prod1_gke_2_region
  zones                      = var.vpc_prod1_gke_2_zones
  network                    = var.vpc_prod1
  subnetwork                 = var.vpc_prod1_subnet_02
  ip_range_pods              = var.vpc_prod1_subnet_02_pods
  ip_range_services          = var.vpc_prod1_subnet_02_svc
  horizontal_pod_autoscaling = true
  network_policy             = true
  node_metadata              = "GKE_METADATA_SERVER"
  identity_namespace         = "${var.project_id}.svc.id.goog"
  monitoring_service         = "monitoring.googleapis.com/kubernetes"
  logging_service            = "logging.googleapis.com/kubernetes"

  node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = var.node_pools_machine_type
      min_count          = 1
      max_count          = 5
      local_ssd_count    = 0
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      # service_account    = "project-service-account@<PROJECT ID>.iam.gserviceaccount.com"
      preemptible        = false
      initial_node_count = 3
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }
}

# p-r1-c-app2 - GKE zonal cluster for apps
module "p-r1-c-app2" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/beta-public-cluster"
  project_id                 = var.project_id
  kubernetes_version         = data.google_container_engine_versions.region_01.latest_master_version
  name                       = var.vpc_prod1_gke_3
  region                     = var.vpc_prod1_gke_3_region
  zones                      = var.vpc_prod1_gke_3_zones
  network                    = var.vpc_prod1
  subnetwork                 = var.vpc_prod1_subnet_03
  ip_range_pods              = var.vpc_prod1_subnet_03_pods
  ip_range_services          = var.vpc_prod1_subnet_03_svc
  horizontal_pod_autoscaling = true
  network_policy             = true
  node_metadata              = "GKE_METADATA_SERVER"
  identity_namespace         = "${var.project_id}.svc.id.goog"
  monitoring_service         = "monitoring.googleapis.com/kubernetes"
  logging_service            = "logging.googleapis.com/kubernetes"

  node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = var.node_pools_machine_type
      min_count          = 1
      max_count          = 5
      local_ssd_count    = 0
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      # service_account    = "project-service-account@<PROJECT ID>.iam.gserviceaccount.com"
      preemptible        = false
      initial_node_count = 3
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }
}


# p-r2-abc-app3 - GKE regional cluster for apps
module "p-r2-abc-app3" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/beta-public-cluster"
  project_id                 = var.project_id
  kubernetes_version         = data.google_container_engine_versions.region_02.latest_master_version
  name                       = var.vpc_prod1_gke_4
  region                     = var.vpc_prod1_gke_4_region
  zones                      = var.vpc_prod1_gke_4_zones
  network                    = var.vpc_prod1
  subnetwork                 = var.vpc_prod1_subnet_04
  ip_range_pods              = var.vpc_prod1_subnet_04_pods
  ip_range_services          = var.vpc_prod1_subnet_04_svc
  horizontal_pod_autoscaling = true
  network_policy             = true
  node_metadata              = "GKE_METADATA_SERVER"
  identity_namespace         = "${var.project_id}.svc.id.goog"
  monitoring_service         = "monitoring.googleapis.com/kubernetes"
  logging_service            = "logging.googleapis.com/kubernetes"

  node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = var.node_pools_machine_type
      min_count          = 1
      max_count          = 5
      local_ssd_count    = 0
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      # service_account    = "project-service-account@<PROJECT ID>.iam.gserviceaccount.com"
      preemptible        = false
      initial_node_count = 2
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }
}

# p-r3-abc-app4 - GKE regional cluster for apps
module "p-r3-abc-app4" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/beta-public-cluster"
  project_id                 = var.project_id
  kubernetes_version         = data.google_container_engine_versions.region_03.latest_master_version
  name                       = var.vpc_prod2_gke_5
  region                     = var.vpc_prod2_gke_5_region
  zones                      = var.vpc_prod2_gke_5_zones
  network                    = var.vpc_prod2
  subnetwork                 = var.vpc_prod2_subnet_05
  ip_range_pods              = var.vpc_prod2_subnet_05_pods
  ip_range_services          = var.vpc_prod2_subnet_05_svc
  horizontal_pod_autoscaling = true
  network_policy             = true
  node_metadata              = "GKE_METADATA_SERVER"
  identity_namespace         = "${var.project_id}.svc.id.goog"
  monitoring_service         = "monitoring.googleapis.com/kubernetes"
  logging_service            = "logging.googleapis.com/kubernetes"

  node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = var.node_pools_machine_type
      min_count          = 1
      max_count          = 5
      local_ssd_count    = 0
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      # service_account    = "project-service-account@<PROJECT ID>.iam.gserviceaccount.com"
      preemptible        = false
      initial_node_count = 2
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }
}


