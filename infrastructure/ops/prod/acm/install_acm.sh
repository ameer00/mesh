# Download ACM operator, copy to GCS
gsutil -q stat gs://${project_id}/ops/acm/config-management-operator.yaml
if [ $? -eq 1 ]; then
    gsutil cp gs://config-management-release/released/latest/config-management-operator.yaml config-management-operator.yaml
    gsutil -m cp config-management-operator.yaml gs://${project_id}/ops/acm/config-management-operator.yaml
else
    echo config-management-operator.yaml already present
    gsutil cp gs://${project_id}/ops/acm/config-management-operator.yaml config-management-operator.yaml
fi

# Create SSH Key pair for Gitlab ACM
gsutil -q stat gs://${project_id}/ops/acm/ssh-keys/acm_gitlab_key
if [ $? -eq 1 ]; then
    ssh-keygen -t rsa -b 4096 -N '' -f acm_gitlab_key
    gsutil -m cp acm_gitlab_key gs://${project_id}/ops/acm/ssh-keys/acm_gitlab_key
    gsutil -m cp acm_gitlab_key.pub gs://${project_id}/ops/acm/ssh-keys/acm_gitlab_key.pub
else
    echo acm_gitlab_key pair already present
    gsutil cp gs://${project_id}/ops/acm/ssh-keys/acm_gitlab_key acm_gitlab_key
    gsutil cp gs://${project_id}/ops/acm/ssh-keys/acm_gitlab_key.pub acm_gitlab_key.pub
fi

# Deploy config management operator to prod clusters
gcloud container clusters get-credentials ${r1_gke1} --region ${region1} --project ${project_id}
gcloud container clusters get-credentials ${r1_gke2} --region ${region1} --project ${project_id}
gcloud container clusters get-credentials ${r1_gke3} --region ${region1} --project ${project_id}
gcloud container clusters get-credentials ${r2_gke4} --region ${region2} --project ${project_id}
gcloud container clusters get-credentials ${r3_gke5} --region ${region3} --project ${project_id}

export r1_gke1_ctx=gke_${project_id}_${region1}_${r1_gke1}
export r1_gke2_ctx=gke_${project_id}_${region1}_${r1_gke2}
export r1_gke3_ctx=gke_${project_id}_${region1}_${r1_gke3}
export r2_gke4_ctx=gke_${project_id}_${region2}_${r2_gke4}
export r3_gke5_ctx=gke_${project_id}_${region3}_${r3_gke5}

kubectl --context ${r1_gke1_ctx} apply -f config-management-operator.yaml
kubectl --context ${r1_gke2_ctx} apply -f config-management-operator.yaml
kubectl --context ${r1_gke3_ctx} apply -f config-management-operator.yaml
kubectl --context ${r2_gke4_ctx} apply -f config-management-operator.yaml
kubectl --context ${r3_gke5_ctx} apply -f config-management-operator.yaml

# Create secrets from SSH private key
kubectl create secret generic git-creds --namespace=config-management-system --from-file=ssh=acm_gitlab_key --dry-run=client -o yaml > git-creds.yaml
kubectl --context ${r1_gke1_ctx} apply -f git-creds.yaml
kubectl --context ${r1_gke2_ctx} apply -f git-creds.yaml
kubectl --context ${r1_gke3_ctx} apply -f git-creds.yaml
kubectl --context ${r2_gke4_ctx} apply -f git-creds.yaml
kubectl --context ${r3_gke5_ctx} apply -f git-creds.yaml

# Create config-management CR for prod clusters
sed -e s/CLUSTER/${r1_gke1}/g -e s/PROJECT_ID/${project_id}/g config/config-management.yaml_tmpl > config-management-${r1_gke1}.yaml
sed -e s/CLUSTER/${r1_gke2}/g -e s/PROJECT_ID/${project_id}/g config/config-management.yaml_tmpl > config-management-${r1_gke2}.yaml
sed -e s/CLUSTER/${r1_gke3}/g -e s/PROJECT_ID/${project_id}/g config/config-management.yaml_tmpl > config-management-${r1_gke3}.yaml
sed -e s/CLUSTER/${r2_gke4}/g -e s/PROJECT_ID/${project_id}/g config/config-management.yaml_tmpl > config-management-${r2_gke4}.yaml
sed -e s/CLUSTER/${r3_gke5}/g -e s/PROJECT_ID/${project_id}/g config/config-management.yaml_tmpl > config-management-${r3_gke5}.yaml

# Deploy config-management CR to prod clusters
kubectl --context ${r1_gke1_ctx} apply -f config-management-${r1_gke1}.yaml
kubectl --context ${r1_gke2_ctx} apply -f config-management-${r1_gke2}.yaml
kubectl --context ${r1_gke3_ctx} apply -f config-management-${r1_gke3}.yaml
kubectl --context ${r2_gke4_ctx} apply -f config-management-${r2_gke4}.yaml
kubectl --context ${r3_gke5_ctx} apply -f config-management-${r3_gke5}.yaml
