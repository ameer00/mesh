# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

################# ACM ######################
# Archive used to detect changes in repo
data "archive_file" "acm_config" {
  type        = "zip"
  source_dir  = "config/"
  output_path = "data.zip"
}

resource "null_resource" "exec_install_acm" {
  provisioner "local-exec" {
    interpreter = ["bash", "-exc"]
    command     = "${path.module}/install_acm.sh"
    environment = {
      project_id = var.project_id
      r1_gke1    = var.r1_gke1
      r1_gke2    = var.r1_gke2
      r1_gke3    = var.r1_gke3
      r2_gke4    = var.r2_gke4
      r3_gke5    = var.r3_gke5
      region1    = var.region1
      region2    = var.region2
      region3    = var.region3
    }
  }
  triggers = {
    script_sha1          = sha1(file("install_acm.sh"))
    data_sha1            = data.archive_file.acm_config.output_sha
  }
}
