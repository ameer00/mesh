git config --global user.email "cloudbuild@qwiklabs.net"
git config --global user.name "cloudbuild"
gsutil cp gs://${project_id}/ops/acm/ssh-keys/acm_gitlab_key acm_gitlab_key
gsutil cp gs://${project_id}/ops/acm/ssh-keys/acm_gitlab_key.pub acm_gitlab_key.pub
chmod 600 acm_gitlab_key
export WORKDIR=`pwd`
export GIT_SSH_COMMAND="ssh -o \"StrictHostKeyChecking=no\" -i ${WORKDIR}/acm_gitlab_key"
git clone git@gitlab.endpoints.${project_id}.cloud.goog:platform-admins/anthos-config-management.git
cp -r acm-repo/* anthos-config-management
cd anthos-config-management
git add .
git commit -m "Initial commit"
git push origin master
