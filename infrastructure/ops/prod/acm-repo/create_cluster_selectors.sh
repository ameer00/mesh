# Copyright 2020 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

declare -a ALL_CLUSTERS
ALL_CLUSTERS=(${r1_gke1} ${r1_gke2} ${r1_gke3} ${r2_gke4} ${r3_gke5} ${r4_gke6} ${r5_gke7})
for cluster in ${ALL_CLUSTERS[@]}
do
    sed -e s/CLUSTER/$cluster/g ./acm-repo/clusterregistry/cluster_selector.yaml_tmpl > ./acm-repo/clusterregistry/selector-$cluster.yaml
done
