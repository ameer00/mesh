# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

data "google_container_engine_versions" "region_04" {
  project        = var.project_id
  location       = var.vpc_dev_stage_gke_6_region
  version_prefix = var.kubernetes_version
}

data "google_container_engine_versions" "region_05" {
  project        = var.project_id
  location       = var.vpc_dev_stage_gke_7_region
  version_prefix = var.kubernetes_version
}

# s-r4-abc-stage1 - GKE regional staging cluster in region 4 for ASM control plane and apps
module "s-r4-abc-stage1" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/beta-public-cluster"
  project_id                 = var.project_id
  kubernetes_version         = data.google_container_engine_versions.region_04.latest_master_version
  name                       = var.vpc_dev_stage_gke_6
  region                     = var.vpc_dev_stage_gke_6_region
  zones                      = var.vpc_dev_stage_gke_6_zones
  network                    = var.vpc_dev_stage
  subnetwork                 = var.vpc_dev_stage_subnet_01
  ip_range_pods              = var.vpc_dev_stage_subnet_01_pods
  ip_range_services          = var.vpc_dev_stage_subnet_01_svc
  horizontal_pod_autoscaling = true
  network_policy             = true
  node_metadata              = "GKE_METADATA_SERVER"
  identity_namespace         = "${var.project_id}.svc.id.goog"
  monitoring_service         = "monitoring.googleapis.com/kubernetes"
  logging_service            = "logging.googleapis.com/kubernetes"

  node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = var.node_pools_machine_type
      min_count          = 1
      max_count          = 3
      local_ssd_count    = 0
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      # service_account    = "project-service-account@<PROJECT ID>.iam.gserviceaccount.com"
      preemptible        = false
      initial_node_count = 2
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }
}

# d-r5-abc-dev1 - GKE regional dev cluster in region 5 for ASM control plane and apps
module "d-r5-abc-dev1" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/beta-public-cluster"
  project_id                 = var.project_id
  kubernetes_version         = data.google_container_engine_versions.region_05.latest_master_version
  name                       = var.vpc_dev_stage_gke_7
  region                     = var.vpc_dev_stage_gke_7_region
  zones                      = var.vpc_dev_stage_gke_7_zones
  network                    = var.vpc_dev_stage
  subnetwork                 = var.vpc_dev_stage_subnet_02
  ip_range_pods              = var.vpc_dev_stage_subnet_02_pods
  ip_range_services          = var.vpc_dev_stage_subnet_02_svc
  horizontal_pod_autoscaling = true
  network_policy             = true
  node_metadata              = "GKE_METADATA_SERVER"
  identity_namespace         = "${var.project_id}.svc.id.goog"
  monitoring_service         = "monitoring.googleapis.com/kubernetes"
  logging_service            = "logging.googleapis.com/kubernetes"

  node_pools = [
    {
      name               = "default-node-pool"
      machine_type       = var.node_pools_machine_type
      min_count          = 1
      max_count          = 3
      local_ssd_count    = 0
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      # service_account    = "project-service-account@<PROJECT ID>.iam.gserviceaccount.com"
      preemptible        = false
      initial_node_count = 2
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }
}

