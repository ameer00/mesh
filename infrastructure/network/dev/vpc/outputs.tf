output "vpc_dev_stage_subnets_self_links" { value = "${module.vpc-dev-stage.subnets_self_links}" }
output "vpc_dev_stage_subnets_names" { value = "${module.vpc-dev-stage.subnets_names}" }
output "vpc_dev_stage_network_name" { value = "${module.vpc-dev-stage.network_name}" }
output "vpc_dev_stage_network_self_link" { value = "${module.vpc-dev-stage.network_self_link}" }
