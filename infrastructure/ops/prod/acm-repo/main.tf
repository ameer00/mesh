# Archive used to detect changes in repo
data "archive_file" "repo" {
  type        = "zip"
  source_dir  = "acm-repo/"
  output_path = "data.zip"
}

resource "null_resource" "exec_create_k8s_repo" {
  provisioner "local-exec" {
    interpreter = ["bash", "-exc"]
    command     = "${path.module}/create_cluster_selectors.sh && ${path.module}/create_cluster_registry.sh && ${path.module}/create_gitlab_runners_configmap.sh && ${path.module}/create_gitlab_runners_secret.sh && ${path.module}/push_acm_repo.sh" 
    environment = {
      project_id = var.project_id
      r1_gke1    = var.r1_gke1
      r1_gke2    = var.r1_gke2
      r1_gke3    = var.r1_gke3
      r2_gke4    = var.r2_gke4
      r3_gke5    = var.r3_gke5
      r4_gke6    = var.r4_gke6
      r5_gke7    = var.r5_gke7
    }
  }

  triggers = {
    script_sha1          = sha1(file("push_acm_repo.sh"))
    script_sha1          = sha1(file("create_cluster_registry.sh"))
    script_sha1          = sha1(file("create_cluster_selectors.sh"))
    script_sha1          = sha1(file("create_gitlab_runners_secret.sh"))
    script_sha1          = sha1(file("create_gitlab_runners_configmap.sh"))
    data_sha1            = data.archive_file.repo.output_sha
  }
}

