/**
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

provider "gitlab" {
  token    = var.gitlab_token
  base_url = "https://${var.gitlab_hostname}/api/v4/"
  insecure = true
}

resource "gitlab_group" "platform-admins" {
  name             = "platform-admins"
  path             = "platform-admins"
  description      = "An group of projects for Platform Admins"
  visibility_level = "internal"
}

resource "gitlab_project" "anthos-config-management" {
  name             = "anthos-config-management"
  description      = "Anthos Config Management repo"
  namespace_id     = gitlab_group.platform-admins.id
  visibility_level = "internal"
  default_branch   = "master"
}

resource "gitlab_deploy_key" "anthos-config-management" {
  project    = "platform-admins/anthos-config-management"
  title      = "acm deploy key"
  key        = file("${path.module}/acm_gitlab_key.pub")
  can_push   = "true"
  depends_on = [gitlab_project.anthos-config-management]
}

resource "gitlab_project" "anthos-service-mesh" {
  name             = "anthos-service-mesh"
  description      = "Anthos Service Mesh repo"
  namespace_id     = gitlab_group.platform-admins.id
  visibility_level = "internal"
  default_branch   = "master"
}

resource "gitlab_deploy_key" "anthos-service-mesh" {
  project    = "platform-admins/anthos-service-mesh"
  title      = "acm deploy key"
  key        = file("${path.module}/acm_gitlab_key.pub")
  can_push   = "true"
  depends_on = [gitlab_project.anthos-service-mesh]
}

resource "gitlab_group_variable" "var-gsa-json" {
   group     = "${gitlab_group.platform-admins.name}"
   key       = "GCP_KEYFILE_JSON_64"
   value     = file("${path.module}/gitlab-sa-base64.json")
   protected = false
   masked    = false
}

resource "gitlab_group_variable" "var-project-id" {
   group     = "${gitlab_group.platform-admins.name}"
   key       = "PROJECT_ID"
   value     = var.project_id
   protected = false
   masked    = false
}

