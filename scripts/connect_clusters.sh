gcloud container clusters get-credentials ${R1_GKE1} --region ${REGION1} --project ${PROJECT_ID}
gcloud container clusters get-credentials ${R1_GKE2} --region ${REGION1} --project ${PROJECT_ID}
gcloud container clusters get-credentials ${R1_GKE3} --region ${REGION1} --project ${PROJECT_ID}
gcloud container clusters get-credentials ${R2_GKE4} --region ${REGION2} --project ${PROJECT_ID}
gcloud container clusters get-credentials ${R3_GKE5} --region ${REGION3} --project ${PROJECT_ID}
gcloud container clusters get-credentials ${R4_GKE6} --region ${REGION4} --project ${PROJECT_ID}
gcloud container clusters get-credentials ${R5_GKE7} --region ${REGION5} --project ${PROJECT_ID}
gcloud container clusters get-credentials gitlab --region ${REGION1} --project ${PROJECT_ID}

kubectl ctx ${R1_GKE1}=gke_${PROJECT_ID}_${REGION1}_${R1_GKE1}
kubectl ctx ${R1_GKE2}=gke_${PROJECT_ID}_${REGION1}_${R1_GKE2}
kubectl ctx ${R1_GKE3}=gke_${PROJECT_ID}_${REGION1}_${R1_GKE3}
kubectl ctx ${R2_GKE4}=gke_${PROJECT_ID}_${REGION2}_${R2_GKE4}
kubectl ctx ${R3_GKE5}=gke_${PROJECT_ID}_${REGION3}_${R3_GKE5}
kubectl ctx ${R4_GKE6}=gke_${PROJECT_ID}_${REGION4}_${R4_GKE6}
kubectl ctx ${R5_GKE7}=gke_${PROJECT_ID}_${REGION5}_${R5_GKE7}
kubectl ctx gitlab=gke_${PROJECT_ID}_${REGION1}_gitlab
