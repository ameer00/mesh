# MAIN
export PROJECT_ID=$(gcloud info --format='value(config.project)')
export ASM_VERSION=1.5.4-asm.2
export ASM_DIR=$HOME/istio-1.5.4-asm.2
export ISTIOCTLCMD=$HOME/istio-$ASM_VERSION/bin/istioctl

# REGIONS
export REGION1='us-central1'
export REGION2='us-west1'
export REGION3='us-west2'
export REGION4='us-east1'
export REGION5='us-east4'

# ZONES
export REGION1_ZONE1='us-central1-a'
export REGION1_ZONE2='us-central1-b'
export REGION1_ZONE3='us-central1-c'
export REGION2_ZONE1='us-west1-a'
export REGION2_ZONE2='us-west1-b'
export REGION2_ZONE3='us-west1-c'
export REGION3_ZONE1='us-west2-a'
export REGION3_ZONE2='us-west2-b'
export REGION3_ZONE3='us-west2-c'
export REGION4_ZONE1='us-east1-b'
export REGION4_ZONE2='us-east1-c'
export REGION4_ZONE3='us-east1-d'
export REGION5_ZONE1='us-east4-a'
export REGION5_ZONE2='us-east4-b'
export REGION5_ZONE3='us-east4-c'

# VPCS
export VPC_PROD1='vpc-prod-1'
export VPC_PROD2='vpc-prod-2'
export VPC_DEV_STAGE='vpc-dev-stage'

# SUBNETS
export VPC_PROD1_SUBNET1_NAME="10-100-4-0-22"
export VPC_PROD1_SUBNET1_DESC="vpc-prod-1-subnet-1"
export VPC_PROD1_SUBNET1_IP='10.100.4.0/22'
export VPC_PROD1_POD_RANGE1_NAME='10-104-0-0-14'
export VPC_PROD1_POD_RANGE1='10.104.0.0/14'
export VPC_PROD1_SVC_RANGE1_NAME='10-100-32-0-24'
export VPC_PROD1_SVC_RANGE1='10.100.32.0/24'

export VPC_PROD1_SUBNET2_NAME='10-108-4-0-22'
export VPC_PROD1_SUBNET2_DESC="vpc-prod-1-subnet-2"
export VPC_PROD1_SUBNET2_IP='10.108.4.0/22'
export VPC_PROD1_POD_RANGE2_NAME='10-112-0-0-14'
export VPC_PROD1_POD_RANGE2='10.112.0.0/14'
export VPC_PROD1_SVC_RANGE2_NAME='10-108-32-0-24'
export VPC_PROD1_SVC_RANGE2='10.108.32.0/24'

export VPC_PROD1_SUBNET3_NAME='10-116-4-0-22'
export VPC_PROD1_SUBNET3_DESC="vpc-prod-1-subnet-3"
export VPC_PROD1_SUBNET3_IP='10.116.4.0/22'
export VPC_PROD1_POD_RANGE3_NAME='10-120-0-0-14'
export VPC_PROD1_POD_RANGE3='10.120.0.0/14'
export VPC_PROD1_SVC_RANGE3_NAME='10-116-32-0-24'
export VPC_PROD1_SVC_RANGE3='10.116.32.0/24'

export VPC_PROD1_SUBNET4_NAME='10-124-4-0-22'
export VPC_PROD1_SUBNET4_DESC="vpc-prod-1-subnet-4"
export VPC_PROD1_SUBNET4_IP='10.124.4.0/22'
export VPC_PROD1_POD_RANGE4_NAME='10-128-0-0-14'
export VPC_PROD1_POD_RANGE4='10.128.0.0/14'
export VPC_PROD1_SVC_RANGE4_NAME='10-124-32-0-24'
export VPC_PROD1_SVC_RANGE4='10.124.32.0/24'

export VPC_PROD2_SUBNET1_NAME='10-132-4-0-22'
export VPC_PROD2_SUBNET1_DESC="vpc-prod-2-subnet-1"
export VPC_PROD2_SUBNET1_IP='10.132.4.0/22'
export VPC_PROD2_POD_RANGE1_NAME='10-136-0-0-14'
export VPC_PROD2_POD_RANGE1='10.136.0.0/14'
export VPC_PROD2_SVC_RANGE1_NAME='10-132-32-0-24'
export VPC_PROD2_SVC_RANGE1='10.132.32.0/24'

export VPC_DEV_STAGE_SUBNET1_NAME='10-100-4-0-22'
export VPC_DEV_STAGE_SUBNET1_DESC="vpc-dev-stage-subnet-1"
export VPC_DEV_STAGE_SUBNET1_IP='10.100.4.0/22'
export VPC_DEV_STAGE_POD_RANGE1_NAME='10-104-0-0-14'
export VPC_DEV_STAGE_POD_RANGE1='10.104.0.0/14'
export VPC_DEV_STAGE_SVC_RANGE1_NAME='10-100-32-0-24'
export VPC_DEV_STAGE_SVC_RANGE1='10.100.32.0/24'

export VPC_DEV_STAGE_SUBNET2_NAME='10-108-4-0-22'
export VPC_DEV_STAGE_SUBNET2_DESC="vpc-dev-stage-subnet-2"
export VPC_DEV_STAGE_SUBNET2_IP='10.108.4.0/22'
export VPC_DEV_STAGE_POD_RANGE2_NAME='10-112-0-0-14'
export VPC_DEV_STAGE_POD_RANGE2='10.112.0.0/14'
export VPC_DEV_STAGE_SVC_RANGE2_NAME='10-108-32-0-24'
export VPC_DEV_STAGE_SVC_RANGE2='10.108.32.0/24'

# CLUSTER NAMES/CONTEXTS
export R1_GKE1='p-r1-abc-ops1'
export R1_GKE2='p-r1-b-app1'
export R1_GKE3='p-r1-c-app2'
export R2_GKE4='p-r2-abc-app3'
export R3_GKE5='p-r3-ab-app4'
export R4_GKE6='s-r4-a-stage1'
export R5_GKE7='d-r5-b-dev1'

# NODE POOLS
export NODE_POOLS_MACHINE_TYPE="e2-standard-4"

# CLUSTER ARRAY
declare -a PROD_CLUSTERS
PROD_CLUSTERS=(
    ${R1_GKE1}
    ${R1_GKE2}
    ${R1_GKE3}
    ${R2_GKE4}
    ${R3_GKE5}
    )
