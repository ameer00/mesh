# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

variable "project_id" {}
variable "vpc_dev_stage" {}

variable "node_pools_machine_type" {}
variable "kubernetes_version" { default = "1.15" }

variable "vpc_dev_stage_gke_6" {}
variable "vpc_dev_stage_gke_6_region" {}
variable "vpc_dev_stage_gke_6_zones" {}

variable "vpc_dev_stage_subnet_01" {}
variable "vpc_dev_stage_subnet_01_pods" {}
variable "vpc_dev_stage_subnet_01_svc" {}

variable "vpc_dev_stage_gke_7" {}
variable "vpc_dev_stage_gke_7_region" {}
variable "vpc_dev_stage_gke_7_zones" {}

variable "vpc_dev_stage_subnet_02" {}
variable "vpc_dev_stage_subnet_02_pods" {}
variable "vpc_dev_stage_subnet_02_svc" {}
