# Download ACM operator, copy to GCS
gsutil -q stat gs://${project_id}/ops/acm/config-management-operator.yaml
if [ $? -eq 1 ]; then
    gsutil cp gs://config-management-release/released/latest/config-management-operator.yaml config-management-operator.yaml
    gsutil -m cp config-management-operator.yaml gs://${project_id}/ops/acm/config-management-operator.yaml
else
    echo config-management-operator.yaml already present
    gsutil cp gs://${project_id}/ops/acm/config-management-operator.yaml config-management-operator.yaml
fi

# Create SSH Key pair for Gitlab ACM
gsutil -q stat gs://${project_id}/ops/acm/ssh-keys/acm_gitlab_key
if [ $? -eq 1 ]; then
    ssh-keygen -t rsa -b 4096 -N '' -f acm_gitlab_key
    gsutil -m cp acm_gitlab_key gs://${project_id}/ops/acm/ssh-keys/acm_gitlab_key
    gsutil -m cp acm_gitlab_key.pub gs://${project_id}/ops/acm/ssh-keys/acm_gitlab_key.pub
else
    echo acm_gitlab_key pair already present
    gsutil cp gs://${project_id}/ops/acm/ssh-keys/acm_gitlab_key acm_gitlab_key
    gsutil cp gs://${project_id}/ops/acm/ssh-keys/acm_gitlab_key.pub acm_gitlab_key.pub
fi

# Deploy config management operator to prod clusters
gcloud container clusters get-credentials ${r4_gke6} --region ${region4} --project ${project_id}
gcloud container clusters get-credentials ${r5_gke7} --region ${region5} --project ${project_id}

export r4_gke6_ctx=gke_${project_id}_${region4}_${r4_gke6}
export r5_gke7_ctx=gke_${project_id}_${region5}_${r5_gke7}

kubectl --context ${r4_gke6_ctx} apply -f config-management-operator.yaml
kubectl --context ${r5_gke7_ctx} apply -f config-management-operator.yaml

# Create secrets from SSH private key
kubectl create secret generic git-creds --namespace=config-management-system --from-file=ssh=acm_gitlab_key --dry-run=client -o yaml > git-creds.yaml
kubectl --context ${r4_gke6_ctx} apply -f git-creds.yaml
kubectl --context ${r5_gke7_ctx} apply -f git-creds.yaml

# Create config-management CR for prod clusters
sed -e s/CLUSTER/${r4_gke6}/g -e s/PROJECT_ID/${project_id}/g config/config-management.yaml_tmpl > config-management-${r4_gke6}.yaml
sed -e s/CLUSTER/${r5_gke7}/g -e s/PROJECT_ID/${project_id}/g config/config-management.yaml_tmpl > config-management-${r5_gke7}.yaml

# Deploy config-management CR to prod clusters
kubectl --context ${r4_gke6_ctx} apply -f config-management-${r4_gke6}.yaml
kubectl --context ${r5_gke7_ctx} apply -f config-management-${r5_gke7}.yaml
