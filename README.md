# Anthos Service Mesh - Multi Cloud Edition

## Architecture diagram

This is what you will build.

## Preparing the environment

1. Create a `WORKDIR` and download the repo inside. All related files to this tutorial end up in `WORKDIR`. This way you can delete the `WORKDIR` at the end of the tutorial.
```
mkdir -p $HOME/mesh && cd $HOME/mesh
git clone https://gitlab.com/ameer00/mesh.git $HOME/mesh/asm && cd $HOME/mesh/asm
```
2. Run the `bootstrap.sh` script in the `scripts` folder. The `bootstrap.sh` script installs the necessary tools (kustomize, pv and kubectl krew plugin). The script also sets up an `infrastructure` CSR repo with a Build Trigger to build the remainder of the infrastructure using Cloud Build and terraform. The location of the `infrastructure` CSR repo folder is `WORKDIR/infra-repo`. Anytime you make a change to the `infrastructure` repo, Cloud Build is triggered which uses the `cloudbuild.yaml` file to build the infrastructure.
```
./scripts/bootstrap.sh
```
> `bootstrap.sh` script is idempotent and may be run multiple times.
