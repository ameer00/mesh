# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

###################  VPC PROD 1 VARIABLES  #######################

# Project ID and VPCs
variable "project_id" {}
variable "vpc_prod_1_name" {}
variable "vpc_prod_2_name" {}
variable "routing_mode" {}

# vpc-prod-1 subnets and secondary subnets
variable "vpc_prod1_subnet_01_name" {}
variable "vpc_prod1_subnet_01_ip" {}
variable "vpc_prod1_subnet_01_region" {}
variable "vpc_prod1_subnet_01_description" {}

variable "vpc_prod1_subnet_02_name" {}
variable "vpc_prod1_subnet_02_ip" {}
variable "vpc_prod1_subnet_02_region" {}
variable "vpc_prod1_subnet_02_description" {}

variable "vpc_prod1_subnet_03_name" {}
variable "vpc_prod1_subnet_03_ip" {}
variable "vpc_prod1_subnet_03_region" {}
variable "vpc_prod1_subnet_03_description" {}

variable "vpc_prod1_subnet_04_name" {}
variable "vpc_prod1_subnet_04_ip" {}
variable "vpc_prod1_subnet_04_region" {}
variable "vpc_prod1_subnet_04_description" {}

variable "vpc_prod1_subnet_01_secondary_svc_1_name" {}
variable "vpc_prod1_subnet_01_secondary_svc_1_range" {}
variable "vpc_prod1_subnet_01_secondary_pod_name" {}
variable "vpc_prod1_subnet_01_secondary_pod_range" {}

variable "vpc_prod1_subnet_02_secondary_svc_1_name" {}
variable "vpc_prod1_subnet_02_secondary_svc_1_range" {}
variable "vpc_prod1_subnet_02_secondary_pod_name" {}
variable "vpc_prod1_subnet_02_secondary_pod_range" {}

variable "vpc_prod1_subnet_03_secondary_svc_1_name" {}
variable "vpc_prod1_subnet_03_secondary_svc_1_range" {}
variable "vpc_prod1_subnet_03_secondary_pod_name" {}
variable "vpc_prod1_subnet_03_secondary_pod_range" {}

variable "vpc_prod1_subnet_04_secondary_svc_1_name" {}
variable "vpc_prod1_subnet_04_secondary_svc_1_range" {}
variable "vpc_prod1_subnet_04_secondary_pod_name" {}
variable "vpc_prod1_subnet_04_secondary_pod_range" {}

# vpc-prod-2 subnets and secondary subnets
variable "vpc_prod2_subnet_01_name" {}
variable "vpc_prod2_subnet_01_ip" {}
variable "vpc_prod2_subnet_01_region" {}
variable "vpc_prod2_subnet_01_description" {}

variable "vpc_prod2_subnet_01_secondary_svc_1_name" {}
variable "vpc_prod2_subnet_01_secondary_svc_1_range" {}
variable "vpc_prod2_subnet_01_secondary_pod_name" {}
variable "vpc_prod2_subnet_01_secondary_pod_range" {}
